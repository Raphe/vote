<?php

require_once 'Model/Votant.php';
require_once 'Model/Elec.php';
require_once 'Vue/Vue.php';

class ControleurVotant {

    private $votant;
    private $elections;

    public function __construct() {
        $this->votant = new Votant();
        $this->elections = new Elec();
    }

    // Affiche les détails sur un votant
    public function detailVotant($idVotant) {
        $votant = $this->votant->getVotant($idVotant);
        $elections = $this->elections->getElecs();
        $alreadyVotes = $this->votant->alreadyVote($idVotant);
        $vue = new Vue("VotantDetail");
        $vue->generer(array('votant' => $votant, 'elections' => $elections, 'alreadyVotes' => $alreadyVotes));
    }

    // Ajoute un commentaire à un billet
    public function createVotant($pseudo) {
        // Sauvegarde du commentaire
        $idVotant = $this->votant->addVotant($pseudo);
        // Actualisation de l'affichage du billet
        $this->detailVotant($idVotant);
    }

    public function coVotant($pseudo) {
        // Vérifie la présence d'un votant et si non, ajoute le votant à la base
        $idVotant = $this->votant->votantExists($pseudo);
        // Actualisation de l'affichage du billet
        header('Location: index.php?action=detail&id='.$idVotant);
        exit;
        //$this->detailVotant($idVotant);
    }

}
