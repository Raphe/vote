<?php

require_once 'Controler/ControleurAccueil.php';
require_once 'Controler/ControleurVotant.php';
require_once 'Vue/Vue.php';
class Routeur {

    private $ctrlAccueil;
    private $ctrlContact;

    public function __construct() {
        $this->ctrlAccueil = new ControleurAccueil();
        $this->ctrlVotant = new ControleurVotant();
    }

    // Route une requête entrante : exécution l'action associée
    public function routerRequete() {
        try {
            if (isset($_GET['action'])) {
                if ($_GET['action'] == 'detail') {
                    $idVotant = intval($this->getParametre($_GET, 'id'));
                    if ($idVotant != 0) {
                        $this->ctrlVotant->detailVotant($idVotant);
                    }
                    else
                        throw new Exception("Identifiant de contact non valide");
                }else if($_GET['action'] == 'connexion'){
                      $pseudo = $this->getParametre($_POST, 'pseudo');
                      $this->ctrlVotant->coVotant($pseudo);
                    }
                else
                    throw new Exception("Action non valide");
            }
            else {  // aucune action définie : affichage de l'accueil
                $this->ctrlAccueil->accueil();
            }
        }
        catch (Exception $e) {
            $this->erreur($e->getMessage());
        }
    }

    // Affiche une erreur
    private function erreur($msgErreur) {
        $vue = new Vue("Erreur");
        $vue->generer(array('msgErreur' => $msgErreur));
    }

    // Recherche un paramètre dans un tableau
    private function getParametre($tableau, $nom) {
        if (isset($tableau[$nom])) {
            return $tableau[$nom];
        }
        else
            throw new Exception("Paramètre '$nom' absent");
    }

}
