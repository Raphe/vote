<?php

require_once 'Model/DB.php';
require_once 'Model/ElecAbs.php';

abstract class ElecFacto
{
  private $bdd;

  public function __construct() {
      $this->bdd = new DB();
  }

    public static function create($idElec)
    {
      $type = getType($idElec);
      if($type==1){
          return new ElecAbs($idElec);
      }else if($type == 2){
          return new ElecMaj($idElec);
      }else{
        throw new Exception("Le type de l'élection identifiant '$idElec' n'est pas reconnu");
      }
    }

    public function getType($idElec) {
        $sql = 'select Type_elec as type'
                . ' FROM election'
                . ' WHERE Id_elec=?';
        $elec = $this->bdd->executerRequete($sql, array($idElec));
        if ($elec->rowCount() > 0)
            $elec = $elec->fetch();
            return $elec['type'];  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucune election ne correspond à l'identifiant '$idElec'");
    }
}
