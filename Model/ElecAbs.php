<?php

require_once 'Model/Elec.php';

/**
 * Fournit les services d'accès aux genres musicaux
 *
 * @author Baptiste Pesquet
 */
class ElecAbs extends Elec {
    private $bdd;

    public function __construct() {
        $this->bdd = new DB();
    }

    public function getElecs() {
        $sql = 'SELECT Id_elec as id,'
                . ' Nom_elec as nom,'
                . ' Statut_elec as statut,'
                . ' Type_elec as type,'
                . ' Resultat_elec as resultat'
                . ' FROM election';
        $elecs = $this->bdd->executerRequete($sql);
        return $elecs;
    }

    public function getElec($idElec) {
        $sql = 'select Id_elec as id,'
                . ' Nom_elec as nom,'
                . ' Statut_elec as statut,'
                . ' Type_elec as type,'
                . ' Resultat_elec as resultat,'
                . ' FROM election'
                . ' WHERE Id_elec=?';
        $elec = $this->bdd->executerRequete($sql, array($idElec));
        if ($elec->rowCount() > 0)
            return $elec->fetch();  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucune election ne correspond à l'identifiant '$idVotant'");
    }

}
