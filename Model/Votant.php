<?php

require_once 'Model/DB.php';
require_once 'Model/Elec.php';

/**
 * Fournit les services d'accès aux genres musicaux
 *
 * @author Baptiste Pesquet
 */
class Votant {
    private $bdd;

    public function __construct() {
        $this->bdd = new DB();
    }
    /** Renvoie la liste des billets du blog
     *
     * @return PDOStatement La liste des billets
     */

    /** Renvoie les informations sur un billet
     *
     * @param int $id L'identifiant du billet
     * @return array Le billet
     * @throws Exception Si l'identifiant du billet est inconnu
     */
    public function getVotant($idVotant) {
        $sql = 'select Id_vot as id,'
                . ' Pseudo_vot as pseudo'
                . ' FROM votant'
                . ' WHERE Id_vot=?';
        $votant = $this->bdd->executerRequete($sql, array($idVotant));
        if ($votant->rowCount() > 0)
            return $votant->fetch();  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucun votant ne correspond à l'identifiant '$idVotant'");
    }

    // Ajoute un contact dans la base
    public function addVotant($pseudo) {
        $sql = 'INSERT INTO votant(Pseudo_vot)'
        . ' values(?)';
        $this->bdd->executerRequete($sql, array($pseudo));
        $idVotant = $this->bdd->lastInsertId();
        return $idVotant;
    }

    public function votantExists($pseudo) {
        $sql = 'select Pseudo_vot as pseudo,'
                . ' Id_vot as id'
                . ' FROM votant'
                . ' WHERE Pseudo_vot= ? ';
        $votant = $this->bdd->executerRequete($sql, array($pseudo));
        if ($votant->rowCount() > 0){
            $votantDetails = $votant->fetch();
            return $votantDetails['id'];
        } // Accès à la première ligne de résultat
        else
            return $this->addVotant($pseudo);
    }

    public function alreadyVote($idVotant) {
        $sql = 'select votes.Id_elec as idElec,'
                . ' election.Nom_elec as nomElec,'
                . ' votes.Id_cand as idCand,'
                . ' candidat.Nom_cand as nomCand'
                . ' FROM votes'
                . ' JOIN election ON votes.Id_elec = election.Id_elec'
                . ' JOIN candidat ON votes.Id_cand = candidat.Id_cand'
                . ' WHERE votes.Id_votant = 1';

        $votes = $this->bdd->executerRequete($sql, array($idVotant));
        return $votes;
    }



}
